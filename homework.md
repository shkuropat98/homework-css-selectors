## CSS-селекторы [Домашнее задание]

## Ответьте на вопросы

#### 1. Что такое селектор и какую роль он выполняет?
> Ответ: Селектор - это первая составляющая CSS правила. Селектор указывает на DOM элемент к которому будет применен стиль.
#### 2. Чем псевдо-классы отличаются от псевдо-элементов?
> Ответ: Псевдокласс позволяет стилизовать элемента в зависимости от внешних факторов.Например наведение мыши на элемент ( hover ). Псевдоэлемент позволяет стилизовать определенную часть элемента. Первую букву строки или первое слово в абзаце или вставить что-то перед элементом(before)
#### 3. Дайте определение специфичности, наследованию и каскаду своими словами.
> Ответ: Специфичность - это что-то типо приоритетности, На один DOM элемент могут указывать несколько CSS правил, но применится правило с большей специфичностью. Наследование - некоторые свойства являются наследуемыми, это значит что потомки получают свойства родителей и эти свойства не нужно явно прописывать. Каскад - правила по которым к элементам применяются CSS стили. Каскад включает в себя специфичность, также для каскада выжен источник CSS правил(Так стили браузера по умолчанию менее приоритетны чем стили определенные пользователем) и очередность в таблице стилей.(элемент ниже имеет приоритет выше) 
#### 4. Для чего кроме адаптации веб-сайтов под разные размеры экранов можно использовать медиа-запросы?
> Ответ:  Для адаптации сайтов под различные типы устройств(принтеры, проекторы, мониторы, скринридеры, терминалы, телевизоры) и параметры экрана(Соотношение сторон экрана, разрешение)
#### 5. Приведите пример сайта, использующего медиа-запросы, изучите при помощи инспектора какие медиа-запросы использованы и как они влияют на поведение сайта на различных устройствах.
> Ответ:  Я смотрел сайт НГТУ. В основном там запросы на изменение макета в зависимоти от разрешения экрана. А версия для слабовидящих представляет собой менюшку которая позволяет настроить цветовую схему сайта.


## Задание #1
Подсчитайте вес селекторов в формате X-Y-Z.

#### 1. `#main * `
> Ответ:{ 1 0 0}
#### 2. `article#article-245 `
> Ответ:{1 0 1}
#### 3. `nav ul > li.active `
> Ответ:{1 1 2}
#### 4. `#navigation a:hover::before `
> Ответ:{1 2 1}
#### 5. `a.button.primary.big[target="_blank"] `
> Ответ: {0 4 1}

## Задание #2
В файле task2.html вам нужно стилизовать горизонтальное меню сайта как в макете. Количество пунктов меню не фиксировано, разделитель должен быть между всеми пунктами меню. Дан готовый шаблон меню (его нельзя менять), нужно подключить файл стилей и с помощью CSS добиться желаемого результата.

Ожидаемый результат: (ваши стили должны работать даже если из меню уберется или добавится несколько пунктов):

![menu](https://ulearn.me/Courses/Frontend.Novosibirsk/U05_css_selectors/menu.png)

* цвет ссылки при наведении `#3072C4`;
* в качестве разделителя нужно использовать символ "♦".

## Задание #3
Так выглядят радиокнопки ([radiobutton](https://developer.mozilla.org/ru/docs/Web/HTML/Element/Input/radio)) в различных браузерах.

![radio](https://ulearn.me/Courses/Frontend.Novosibirsk/U05_css_selectors/radio-1.png)

![radio](https://ulearn.me/Courses/Frontend.Novosibirsk/U05_css_selectors/radio-2.png)

![radio](https://ulearn.me/Courses/Frontend.Novosibirsk/U05_css_selectors/radio-3.png)

Как вы видете, с ними есть одна проблема: в разных браузерах они выглядят по-разному. Но нам (и дизайнерам Контура) хочется чтобы все пользователи видели одинаковые радиокнопки независимо от браузера.

Дан готовый шаблон меню (его нельзя менять), нужно подключить файл стилей и с помощью CSS добиться желаемого результата.

Ваша задача: стилизовать радиокнопки по Контур.гайдам:

![radio](https://ulearn.me/Courses/Frontend.Novosibirsk/U05_css_selectors/radio.png)

Дан готовый шаблон формы (его нельзя менять), пишем только CSS:

* связь между радиокнопкой и лейблом осуществляется через атрибуты `id` и `for`;
* объединять радиокнопки в группы помогает атрибут `name`;
* обратите внимание на атрибуты [checked](http://htmlbook.ru/html/input/checked), [disabled](http://htmlbook.ru/html/input/disabled) и на псевдоселекторы [:checked](http://htmlbook.ru/css/checked) и [:disabled](http://htmlbook.ru/css/disabled);
* необходимо скрыть стандартную радиокнопку, так как ее стиль задан браузером и на ее внешний вид нельзя повлиять;
* нельзя использовать изображения (для стилизации потребуются псевдоэлементы `::before` и `::after`);
* для псевдоэлементов `::before` и `::after` обязательно нужно указывать CSS-свойство `content` (если нужно добавить блок без текстового содежимого, то в качестве значения нужно указать пустую строку **content: '';**), иначе псевдоэлемент не будет отображен;
* для стилизации радиокнопок будет достаточно CSS-свойств `background-color`, `border`, `border-radius`, `opacity`. Информацию о них можно найти в [справочнике CSS](http://htmlbook.ru/css).
